<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>phptest</title>
    <link rel="stylesheet" href="dist/node_modules/bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" href="dist/css/style.css">
</head>
<body>
    <section>
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <form action="store.php" method="post">
                <table class="table">
                    <tr>
                        <td><label for="yourname">Name</label></td>
                        <td><input type="text" name="name" class="form-control" id="yourname" placeholder="Your name" required></td>
                    </tr>
                    <tr>
                        <td><label for="emailaddress">Email</label></td>
                        <td><input type="email" name="email" class="form-control" id="emailaddress" placeholder="Your mail"></td>
                    </tr>
                    <tr>
                        <td><label for="inputaddress">Address</label></td>
                        <td><input type="text" name="address" class="form-control" id="inputaddress" placeholder="Your address" required></td>
                    </tr>
                    <tr>
                        <td><label for="mobilenumber">Mobile</label></td>
                        <td><input type="tel" name="mobile" class="form-control" id="mobilenumber" placeholder="phone number" required></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><button class="btn btn-default" type="submit">send</button></td>
                    </tr>
                </table>
            </form>
        </div>
        <div class="col-md-4"></div>
    </section>
</body>
</html>