<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="dist/css/style.css">
</head>
<style>
    @import url('https://fonts.googleapis.com/css?family=Roboto&display=swap');
    /* font-family: 'Roboto', sans-serif; */

    .show-tbl {
        width: 70%;
        margin: 0 auto;
        background: #cccccc;
        min-height: 150px;
        padding: 0px 20px 20px;
        box-sizing: border-box;
    }
    .show {
        width: 100%;
    }
    .show caption {
        color: #000;
        font-size: 22px;
        font-weight: bold;
        padding: 10px 0;
    }
    .show, th, td {
        border-collapse: collapse;
        border: 1px solid black;
    }
    .show th, td {
        padding: 5px 10px;
    }
    .show th {
        color:  #000;
    }
    .col-center {
        text-align: center;
    }

</style>
<body style="font-family: 'Roboto', sans-serif;">

<?php
    $conn = mysqli_connect("localhost","root","","hospital");  
    $select = "SELECT * FROM person";
    $table = $conn->query($select);
?>
<div class="show-tbl">
    <table class="show">
        <caption>Patient contact list</caption>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Email</th>
            <th>Address</th>
            <th>Mobile</th>
        </tr>
    <?php
        while($row = $table->fetch_assoc()) {
    ?>
        <tr>
            <td class="col-center"><?php echo $row["id"] ?></td>
            <td><?php echo $row["name"] ?></td>
            <td><?php echo $row["email"] ?></td>
            <td><?php echo $row["address"] ?></td>
            <td class="col-center"><?php echo $row["mobile"] ?></td>
        </tr>
    <?php  } ?>
    </table>
</div>

</body>
</html>